import Data.Maybe
import Control.Concurrent
import Control.Concurrent.STM
import Control.Exception
import Control.Monad
import Control.Monad.IO.Class
import qualified Data.Text as T
import qualified Network.WebSockets as WS
import Math.Noise

type Client = WS.Sink WS.Hybi00

type Grid = [[Int]]

data ServerState = ServerState {
    gridT :: TVar Grid,
    clientsT :: TVar [Client],
    chan :: TChan String
}

genGrid :: Int -> Grid
genGrid seed = map (map (\(x,y) -> val (fromJust $ getValue n (x, y, 0)))) u
    where
        val d = if d > 0 then 0 else 1
        n = gen $ perlin { perlinSeed = seed }
        u = [[(x,y) | x <- genUniformDistribution 30] | y <- genUniformDistribution 30]

main :: IO ()
main = do
    grid <- newTVarIO $ genGrid 1
    clients <- newTVarIO []
    chan_ <- newTChanIO

    let state = ServerState grid clients chan_

    forkIO $ gameLoop state
    WS.runServer "0.0.0.0" 9160 $ newClient state

newClient :: ServerState -> WS.Request -> WS.WebSockets WS.Hybi00 ()
newClient state rq = do
    WS.acceptRequest rq

    liftIO $ putStrLn "new client"
    sink <- WS.getSink

    liftIO $ atomically $ do
        clients <- readTVar $ clientsT state
        writeTVar (clientsT state) (sink:clients)

    WS.catchWsError
        (forever $ do
            msg <- WS.receiveData
            liftIO $ atomically $ writeTChan (chan state) $ T.unpack msg)
        (\e -> case fromException e of
            Just WS.ConnectionClosed -> liftIO $ do
                atomically $ do
                    clients <- readTVar $ clientsT state
                    writeTVar (clientsT state) (filter (/= sink) clients)
                return ()
            _ -> return ())

broadcast :: ServerState -> String -> IO ()
broadcast state _ = do
    grid <- atomically $ readTVar $ gridT state
    let wholemsg = show grid
    putStrLn $ "sending " ++ wholemsg
    clients_ <- atomically $ readTVar (clientsT state)
    forM_ clients_ $ \(sink) -> WS.sendSink sink $ WS.textData $ T.pack wholemsg

gameLoop :: ServerState -> IO ()
gameLoop state = forever $ do
    input <- atomically $ readTChan (chan state)
    putStrLn $ "received: " ++ input
    broadcast state input
